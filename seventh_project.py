import pygame
import random
from confic import *
from unit import Units, Scout, Settler
from maps import Map, Tile

def setScreen():
    pygame.display.set_caption("CiviliZ")    
    screen = pygame.display.set_mode((SCREENWIDTH, SCREENHEIGHT))
    return screen

def getRandomType():
    seed = random.random()
    if 0 <= seed < 0.45:
        ID = 0 #DIRT
    if 0.45 <= seed < 0.65:
        ID = 1 #WATER
    if 0.65 <= seed < 0.7:
        ID = 2 #MOUNTAIN
    if 0.7 <= seed < 1:
        ID = 3 #FOREST
    return ID

def main():
    pygame.init()
    screen = setScreen()
    map_ = Map(MAPTILEWIDTH, MAPTILEHEIGHT)
    in_loop = True    

    scouts = [Units(2, 3), Scout(6, 3), Scout(2, 5), Scout(6, 5), Scout(3, 6), Scout(4, 6), Scout(5, 6)]
    settler = Settler(1, 1)

    while in_loop:
        map_.draw(screen)
        [i.draw(screen) for i in scouts]
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                in_loop = False
                pygame.display.quit()
                continue
            elif event.type == pygame.MOUSEBUTTONDOWN:
                [i.click(pygame.mouse.get_pressed(), event.pos, screen) for i in scouts]
                settler.click(pygame.mouse.get_pressed(), event.pos, screen)
        if not in_loop:
            continue 
        settler.draw(screen)
        pygame.display.update()    

main()
