import pygame
from confic import *

class Units(pygame.sprite.Sprite):
    def __init__(self, x, y):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.Surface((normalSide, normalSide))
        self.image.blit(pygame.image.load('scout.png'), (0, 0))
        self.rect = pygame.Rect(x*normalSide, y*normalSide, normalSide, normalSide)
        self.pressed = False
        self.file_name = 'scout.png'
        self.click_file_name = 'scout_pressed.png'
        self.step = 5
        self.select_frames = []

    def draw(self, screen):
        screen.blit(self.image, (self.rect.x, self.rect.y))
        frame = pygame.image.load("frame.png")
        for x, y in self.select_frames:
            screen.blit(frame, (x, y))

    def click(self, key, pos, screen):
        if key == (1, 0, 0):
            if self.pressed:
                self.image.blit(pygame.image.load(self.file_name), (0, 0))
                self.warp(pos, screen)
                self.pressed = False
            else:
                if pos[0] > self.rect.x and pos[0] < self.rect.x + normalSide and pos[1] > self.rect.y and pos[1] < self.rect.y + normalSide:
                    self.image.blit(pygame.image.load(self.click_file_name), (0, 0))
                    self.pressed = True
            self.start_generate_frame()

    def warp(self, pos, screen):
        dx = (pos[0]-self.rect.x) // normalSide
        dy = (pos[1]-self.rect.y) // normalSide
        if abs(dx) + abs(dy) <= self.step:
            self.rect.x += dx * normalSide
            self.rect.y += dy * normalSide
            self.draw(screen)

    def start_generate_frame(self):
        if self.pressed:
            self.generate_frame(self.step, self.rect.x, self.rect.y)
        else:
            self.select_frames = []

    def generate_frame(self, step, x, y):
        self.select_frames.append((x, y))
        if step > 0:
            new_x = x + normalSide
            if new_x < SCREENHEIGHT:
                self.generate_frame(step - 1, new_x, y)
            new_y = y + normalSide
            if new_y < SCREENWIDTH:
                self.generate_frame(step - 1, x, new_y)
            new_x = x - normalSide
            if new_x >= 0:
                self.generate_frame(step - 1, new_x, y)
            new_y = y - normalSide
            if new_y >= 0:
                self.generate_frame(step - 1, x, new_y)
           

class Settler(Units):
    def __init__(self, x, y):
        Units.__init__(self, x, y)
        self.image.blit(pygame.image.load('settler.png'), (0, 0))
        self.file_name = 'settler.png'
        self.click_file_name = 'settler_pressed.png'
        self.step = 2

class Scout(Units):
    def __init__(self, x, y):
        Units.__init__(self, x, y)
        self.image.blit(pygame.image.load('scout.png'), (0, 0))
        self.file_name = 'scout.png'
        self.click_file_name = 'scout_pressed.png'
        self.step = 5
