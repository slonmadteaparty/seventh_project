import pygame
import random
from confic import *

class Tile:
    typeToColor = {0:"#cf8b14", 1:"#004e6f", 2:"#7f7f7f", 3:"#003100"} 
    def __init__(self, tileType):
        self.TILE_SIDE = min(SCREENWIDTH // MAPTILEWIDTH, SCREENHEIGHT // MAPTILEHEIGHT)
        self.ID = tileType
        self.color = self.typeToColor[tileType]            
        self.surface = pygame.Surface((self.TILE_SIDE, self.TILE_SIDE))
        self.surface.fill(pygame.Color(self.color))

class Map:
    def __init__(self, tile_x, tile_y):
        self.tile_x = tile_x
        self.tile_y = tile_y
        self.map_ = []
        for i in xrange(MAPTILEWIDTH):
            self.map_.append([])
            for j in xrange(MAPTILEHEIGHT):
                self.map_[i].append(Tile(getRandomType(), ))

    def draw(self, screen):
        for i in range(len(self.map_)):
            for j in range(len(self.map_[i])):
                screen.blit(self.map_[i][j].surface, (i * self.map_[i][j].TILE_SIDE, j * self.map_[i][j].TILE_SIDE))

def getRandomType():
    seed = random.random()
    if 0 <= seed < 0.45:
        ID = 0 #DIRT
    if 0.45 <= seed < 0.65:
        ID = 1 #WATER
    if 0.65 <= seed < 0.7:
        ID = 2 #MOUNTAIN
    if 0.7 <= seed < 1:
        ID = 3 #FOREST
    return ID

